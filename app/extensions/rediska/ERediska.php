<?php

Yii::import('ext.rediska.Rediska');

/**
 * Class ERediska
 */
class ERediska extends CComponent
{
    public static $options = array(
        'namespace' => 'Application_',
        'servers' => array(
            array('host' => '127.0.0.1', 'port' => '6379')
        )
    );

    private static $rediska;

    public static function client()
    {
        if(Yii::app()->params['rediska']) {
           $options = Yii::app()->params['rediska'];
        } else {
           $options = self::$options;
        }

        if(isset(self::$rediska)) {
            return self::$rediska;
        } else {
            self::$rediska = new Rediska($options);
            return self::$rediska;

        }
    }
}