<?php

class SiteController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionCreateLink($link)
    {
        $hash = md5($link.time().rand());

        ERediska::client()->set($hash,$link);

        $newLink = Yii::app()->createAbsoluteUrl('/l/'.$hash);
        $this->render('link', array('link' => $newLink));
    }

    public function actionShowLink($hash)
    {
        $link = ERediska::client()->get($hash);

        if($link === null) {
            $this->render('nolink');
        }

        ERediska::client()->delete($hash);
        $this->redirect($link);

    }

    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            $this->render('error', $error);
        }
    }
}