<?php $this->pageTitle = "Quicklinkr"?>

<div class="jumbotron">
    <h1>ONE TIME LINKS</h1>

    <p class="lead">Do you want show something to only one person? Make a link and give it him. This link will be available only one time.</p>

    <form class="form-inline" method="GET" role="form" action="<?php echo Yii::app()->createUrl('/site/createLink'); ?>">
        <div class="form-group">
            <input type="url" id="link-input" name="link" class="form-control" placeholder="Put your link here...">
        </div>
        <input type="submit" class="btn btn-lg btn-success" value="Get new link">
    </form>
</div>