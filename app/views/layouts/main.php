<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title><?php echo $this->pageTitle; ?></title>

    <link href="<?php echo Yii::app()->baseUrl ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl ?>/css/jumbotron.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl ?>/css/main.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div class="header">
        <ul class="nav nav-pills pull-right">
            <li class="active"><a href="/">Home</a></li>
<!--            <li><a href="#">About</a></li>-->
<!--            <li><a href="#">Contact</a></li>-->
        </ul>
        <h3 class="text-muted" id="appname"><?php echo Yii::app()->name ?></h3>
    </div>

    <?php echo $content; ?>

    <div class="footer">
        <p class="centered">made by <a href="http://krvital.pro/">krvital</a></p>
    </div>

</div>
</body>
</html>
